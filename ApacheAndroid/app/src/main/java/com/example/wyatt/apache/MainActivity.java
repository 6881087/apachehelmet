package com.example.wyatt.apache;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.Header;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.UnsupportedEncodingException;


public class MainActivity extends ActionBarActivity {
    private float[] magneticFieldValues = new float[3];
    private float[] accelerometerValues = new float[3];
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private Sensor aSensor;
    private TextView tvHeading;
    private EditText HostTextBox;
    private int previousOrientation = 0;
    private RequestClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        tvHeading = (TextView) findViewById(R.id.textView);
        HostTextBox = (EditText) findViewById(R.id.editText);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        aSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(myListener, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(myListener, aSensor, SensorManager.SENSOR_DELAY_NORMAL);
        previousOrientation = Math.round(calculateOrientation());
        client = new RequestClient();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onPause(){
        mSensorManager.unregisterListener(myListener);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    float calculateOrientation() {
        float[] values = new float[3];
        float[] R = new float[9];
        SensorManager.getRotationMatrix(R, null, accelerometerValues, magneticFieldValues);
        SensorManager.getOrientation(R, values);
        values[0] = (float) Math.toDegrees(values[0]);
        return values[0];
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    final SensorEventListener myListener = new SensorEventListener() {
           int MaxSum = 20;
           int sumdiff = 0;
           int counter = 0;
        public void onSensorChanged(SensorEvent sensorEvent) {
            int diff = 0;
            int currentOrientation = 0;
            int sign = 1;

            if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
                magneticFieldValues = sensorEvent.values;
            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
                accelerometerValues = sensorEvent.values;
            currentOrientation = Math.round(calculateOrientation());
            diff = smallSideRotation(currentOrientation - previousOrientation);
            previousOrientation = currentOrientation;
            if (counter < MaxSum) {
                sumdiff+= diff;

                //   tvHeading.setText("Heading: " + Float.toString(currentOrientation) + " degrees");
                ++counter;
            } else {
                if (Math.abs(sumdiff) > 10 ) {
                    sumdiff = smallSideRotation(sumdiff);
                    sendData(sumdiff);
                    Log.d("ZZZ", "Send " + sumdiff);
                }

                sumdiff = 0;
                counter = 0;
            }
        }
        public void onAccuracyChanged(Sensor sensor, int accuracy) {}
        private int smallSideRotation(int diff) {
            int sign = 1;
            /// /small side rotate
            if (Math.abs(diff) > 180) {
                sign = diff < 0 ? 1 : -1;
                diff = (360 - Math.abs(diff)) * sign;
            }
            return diff;
        }
    };



    private void sendData( int diff) {
        long time= System.currentTimeMillis();

        String url = "/";
        RequestParams par = new RequestParams();
        String r = diff > 0 ? "1" : "0";
        par.put("s", r);
        par.put("d", String.valueOf(Math.abs(diff)));

        int i = 0;
        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {

            }
            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable var4){

            }
        };
        try {
              client.get(url, par, handler);
        } catch (Exception e) {

            e.printStackTrace();

        }
    }




}
